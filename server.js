const express = require('express')
const app = express()
const PORT = 1981
const Constants = require('./private/constants')
const AuthProvider = require('./services/AuthProvider')
const SpreadsheetService = require('./services/SpreadsheetService')
const GoogleDriveService = require('./services/GoogleDriveService')
const MailingService = require('./services/MailingService')
const Scheduler = require('./services/Scheduler')

app.listen(PORT, () => {
	console.info(`API listening on ${PORT}`)
})
