const fs = require('fs')
const readline = require('readline')
const { google } = require('googleapis')

class AuthProvider {
	constructor() {
		this.scopes = [
			'https://www.googleapis.com/auth/spreadsheets',
			'https://www.googleapis.com/auth/drive',
		]
		this.basePrivatePath = process.cwd() + '/private'
		this.tokenPath = this.basePrivatePath + '/token.json'
		this.credentialsPath = this.basePrivatePath + '/credentials.json'

		this.auth = null
	}

	connect() {
		return new Promise((resolve, reject) => {
			fs.readFile(this.credentialsPath, (err, content) => {
				if (err) reject(err)

				this._authorizeClient(JSON.parse(content), auth => {
					this.auth = auth
					resolve(this.auth)
				})
			})
		})
	}

	get() {
		return this.auth
	}

	_askNewToken(oAuth2Client, callback) {
		const authUrl = oAuth2Client.generateAuthUrl({
			access_type: 'offline',
			scope: this.scopes,
		})
		console.log(`URL pour autoriser l'application:`, authUrl)

		const rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout,
		})
		rl.question(`\nCode d'autorisation: `, code => {
			rl.close()
			oAuth2Client.getToken(code, (err, token) => {
				if (err)
					return console.error('Erreur pendant la récupération du token', err)
				oAuth2Client.setCredentials(token)

				// Sauvegarder le token pour réutilisation
				fs.writeFile(this.tokenPath, JSON.stringify(token), err => {
					if (err) return console.error(err)
					console.info(
						`Token sauvegardé à l'adresse suivante: `,
						this.tokenPath,
					)
				})
				callback(oAuth2Client)
			})
		})
	}

	_authorizeClient(credentials, callback) {
		if (!(credentials && credentials.installed))
			return this._throwError(
				'Les identifiants contenus dans le JSON ne sont pas bien formattés!',
			)

		const { client_secret, client_id, redirect_uris } = credentials.installed
		const oAuth2Client = new google.auth.OAuth2(
			client_id,
			client_secret,
			redirect_uris[0],
		)

		// Check if we have previously stored a token.
		fs.readFile(this.tokenPath, (err, token) => {
			if (err) return this._askNewToken(oAuth2Client, callback)
			oAuth2Client.setCredentials(JSON.parse(token))
			callback(oAuth2Client)
		})
	}
}

module.exports = new AuthProvider()
