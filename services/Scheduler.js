const schedule = require('node-schedule')
const parse = require('date-fns/parse')
const subHours = require('date-fns/sub_hours')

class Scheduler {
  constructor() {
    this.jobs = []
  }

  on(dateStr, next) {
    const hoursLeft = 48 /* Alert 48 hours before */
    const alertDate = subHours(parse(dateStr), hoursLeft)

    this.jobs.push([ schedule.scheduleJob(alertDate, next) ])
  }
}

module.exports = new Scheduler()
