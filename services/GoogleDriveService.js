const async = require('async')
const { google } = require('googleapis')

class GoogleDriveService {
	constructor() {
		this.driveService = null
	}

	init(auth) {
		this.driveService = google.drive({ version: 'v3', auth })
		return this
	}

	share(fileId, permissions) {
		const response = []

		return new Promise((resolve, reject) => {
			async.eachSeries(
				permissions,
				(permission, permissionCallback) => {
					this.driveService.permissions.create(
						{ resource: permission, fileId, fields: 'id' },
						(err, res) => {
							if (err) {
								permissionCallback(err)
								response.push({
									success: false,
									message: err,
								})
							} else {
								permissionCallback()
								response.push({
									success: true,
									message: res,
								})
							}
						},
					)
				},
				err => {
					if (err) {
						response.push({
							success: false,
							message: err,
						})

						reject(response)
					} else {
						response.push({
							success: true,
							message: 'Toutes les permissions ont été accordées.',
						})

						resolve(response)
					}
				},
			)
		})
	}

	_gateway() {
		if (!this.driveService) {
			return this._throwError(
				'Could not operate this action; API is not authenticated or DriveService not installed !',
			)
		}
	}

	_throwError(error) {
		console.error(error)
		return false
	}
}

module.exports = new GoogleDriveService()
