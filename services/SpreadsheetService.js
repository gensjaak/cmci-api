const { google } = require('googleapis')
const PARTICIPANTS_SHEET_TITLE = 'Participants'

class SpreadsheetService {
	constructor() {
		this.sheetsService = null
	}

	init(auth) {
		this.sheetsService = google.sheets({ version: 'v4', auth })
		return this
	}

	create(title, next) {
		this._gateway()

		return new Promise((resolve, reject) => {
			const resource = {
				properties: { title },
				sheets: [{ properties: { title: PARTICIPANTS_SHEET_TITLE } }],
			}

			this.sheetsService.spreadsheets.create({ resource }, (err, result) => {
				if (err) {
					reject(err)
				} else {
					resolve(result.data)
				}
			})
		})
	}

	push(spreadsheetId, data) {
		this._gateway()

		return new Promise((resolve, reject) => {
			this.sheetsService.spreadsheets.values.append(
				{
					spreadsheetId,
					range: `${PARTICIPANTS_SHEET_TITLE}!A1`,
					valueInputOption: 'RAW',
					resource: { values: [[...data]] },
				},
				(err, result) => {
					if (err) {
						reject(err)
					} else {
						resolve(result)
					}
				},
			)
		})
	}

	_gateway() {
		if (!this.sheetsService) {
			return this._throwError(
				'Could not operate this action; API is not authenticated or SheetsService not installed !',
			)
		}
	}

	_throwError(error) {
		console.error(error)
		return false
	}
}

module.exports = new SpreadsheetService()
