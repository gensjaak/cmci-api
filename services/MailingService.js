const nodemailer = require('nodemailer')

class MailingService {
  constructor() {
    this.password = `kaPA-8IWA-xASA_azwu-HHAZ-12JA-12KS`
    this.from = `cmci.bethel@gmail.com`

    this.transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: this.from,
        pass: this.password,
      },
    })
  }

  // Send mail
  _sendMail(data) {
    this.transporter.sendMail(data, (error, info) => {
      if (error) console.error(error)
      else console.log('Mail envoyé aux destinataires.', info.envelope)
    })
  }

  // Verify
  verify() {
    return new Promise((resolve, reject) => {
      this.transporter.verify((error, success) => {
        if (error) reject(error)
        else resolve(success)
      })
    })
  }

  // Broadcast event approaching
  broadcast(subject, greetings, text, contacts = []) {
    const daysLeft = 2

    contacts.forEach(contact => {
      if (contact && contact.email)
        this._sendMail({
          from: this.from,
          to: contact.email,
          subject: `${subject} - Plus que ${daysLeft} jours!`,
          text: `
            ${greetings} ${contact.fullName},
            ${text}
          `,
        })
    })
  }
}

module.exports = new MailingService()
